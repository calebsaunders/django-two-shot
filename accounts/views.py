from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LoginForm
from django.urls import reverse

# Create your views here.

from django.db import IntegrityError

from django.contrib.auth.models import User
from django.db import IntegrityError


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            password_confirmation = form.cleaned_data.get(
                "password_confirmation"
            )

            if password == password_confirmation:

                # Check if a user with the same username already exists
                # if User.objects.filter(username=username).exists():
                #     raise IntegrityError(
                #         f"Username '{username}' already exists."
                #      )

                # Create a new user
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("home")

            else:
                form.add_error(None, "The passwords do not match.")
    else:
        form = SignUpForm()

    context = {"form": form}
    return render(request, "accounts/signup.html", context)


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")
