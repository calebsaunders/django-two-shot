from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

# Register your models here.
admin.site.register(Account)
admin.site.register(ExpenseCategory)
admin.site.register(Receipt)


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "owner")


class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "owner")


class ReceiptAdmin(admin.ModelAdmin):
    list_display = ("vendor", "total", "tax", "date", "category", "account")
