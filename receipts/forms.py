from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account
from .widgets import DateTimePicker
from django import forms


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "tax",
            "category",
            "account",
            "date",
            "total",
        ]
        datetime_field = forms.DateTimeField(widget=DateTimePicker)


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
