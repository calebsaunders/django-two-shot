from django.shortcuts import render, redirect
from django.db.models import Count
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url="login")
def list_receipts(request):
    if request.user.is_authenticated:

        receipts = Receipt.objects.filter(purchaser=request.user)
        context = {"receipts": receipts}
        return render(request, "receipts/list_receipts.html", context)
    else:
        return redirect("/login")


@login_required(login_url="login")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required(login_url="login")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required(login_url="/login")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)


@login_required(login_url="/login")
def category_list(request):
    categories = ExpenseCategory.objects.annotate(
        receipt_count=Count("receipts")
    )
    context = {
        "categories": categories,
    }
    return render(request, "receipts/category_list.html", context)


@login_required(login_url="/login")
def account_list(request):
    accounts = Account.objects.annotate(receipt_count=Count("receipts"))
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/account_list.html", context)
