from django import forms


class DateTimePicker(forms.DateTimeInput):
    input_type = "datetime-local"
